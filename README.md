# restIZ-v1

restIZ allows to know the borough and the business unit of a worker who informs his address in the application


## Installation and Usage

### Base 
 - git clone the repository
### Api

```api

IN local :

1. cd ./restiz-v1/api folder

2. npm install [for dependencies]

3. with heroku install 
[Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

4.launch in local, command :

heroku local

5. see api documentation to localhost:8000/app-docs you can try by
adding number of the street, street name, zipcode and city parameters

OR

You can test the api to retrieve business unit and district name by changing the fields numberOFTheStreet, 
streetName, zipCode and cityName, in the folowing url

http://localhost:8000/api/public/workers/adress/numberOFTheStreet/streetName/zipCode/cityName

for example :

http://localhost:8000/api/public/workers/adress/60/rue+Yvan+Tourgueniev/78380/Bougival

warning other port 8001 !


IN cloud:

1. access to https://api-restiz-v1.herokuapp.com/

2. see api documentation to https://api-restiz-v1.herokuapp.com/app-docs , you can try by
adding number of the street, street name, zipcode and city parameters

```

![swagger](https://media.giphy.com/media/kDZ8T4ZG6qqPLgZORD/giphy.gif)

OR

```
3. test the api to retrieve business unit and district name

https://api-restiz-v1.herokuapp.com/api/public/workers/adress/numberOFTheStreet/streetName/zipCode/cityName

for example :

https://api-restiz-v1.herokuapp.com/api/public/workers/adress/60/rue+Yvan+Tourgueniev/78380/Bougival
```
![api-brut](https://media.giphy.com/media/XHeECeBEczUE3fC6ZO/giphy.gif)

### Front
```front
IN local :

1. cd ./restiz-v1/web folder

2. npm install [for dependencies]

3. with heroku install 
[Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

4.launch in local, command :

heroku local

5. try to submit  a form to retrieve business unit and district name at http://localhost:5000/
(if you have an app running at port 5000 check the port when you will launch the command heroku local)

In cloud:

1. submit a form to retrieve business unit and district name, try it https://web-restiz-v1.herokuapp.com/
```

![front-gif](https://media.giphy.com/media/hv3dq9TCXVx1uXByzL/giphy.gif)



## Improvement

- Currently checking the name of the street and the number of it is made based on the length of the characters and whether it is a number or a string, while the correspondence between the postal code and the filled city is well verified via the combination of the two APIs, it would be necessary to push the verification further and check if for the city and the postal code in question there is indeed a name of street and an existing number

- Differents environnement to facilitate deployment

- Also an manager errors in front

- In protected routes implements the logic (for example check if there is a token with each client request)

## Stack
 🎉
```front & api
- React
- Redux
- Material UI
- External api : https://data.opendatasoft.com/api and https://public.opendatasoft.com/api
- Axios
- NodeJS
- Swagger-ui-express
- ...
```
## License
[None](https://choosealicense.com/licenses/none/)
