import cors from 'cors'
import corsOptions from '../config/cors'
import getWorkerAdressInfo from '../services/workerAdress.services'

//get worker businessUnit and district name passing 4 parameters to url
const workerAdressRoutes = (app) => {
    app.get('/api/public/workers/adress/:numberStreet/:streetName/:zipCode/:city', cors(),getWorkerAdressInfo )
}

export default workerAdressRoutes