import dotenv from 'dotenv'
dotenv.config()

/**
 * to acess to api documentation add api-docs/ after the actual url
 * @param {*} app 
 */
const InitialRoutes = (app) => {
    app.get('/', (req, res) => { 
        res.send(`Welcome to the api you can acess to the api documentation /api-docs/`)
    } )
}

export default InitialRoutes