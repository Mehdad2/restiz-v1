import axios from 'axios'
import manageErrors from '../utils/manageError'
import errors from '../utils/errors'

/**
 * Manage to get business unit and district name according to region info
 * @param {*} regionInfo 
 * @param {*} nameDistrict 
 */
const businessUnitInfo = (regionInfo, nameDistrict) => {
    const data = (regionName) => {
         return {
            businessUnit: regionName,
            districtName: nameDistrict
        }
    }
    if(regionInfo === "ILE-DE-FRANCE") {
        return data(regionInfo)
    }
    if(regionInfo === "PROVENCE-ALPES-COTE D'AZUR") {
        return data(regionInfo)
    }
    return data("AUTRES")


}

/**
 * worker info, we call two externals api to get the data we want,
 * the first one cointain all districts_code of a city and the second contained the ditrict name 
 * that we can retrieve thanks to the concatenation beetween the first two digits of a zipcode with the district code
 * @param {*} req 
 * @param {*} res 
 */

const getWorkerAdressInfo = (req, res) => {
    const {numberStreet, streetName, zipCode, city } = req.params
    const cityLength = city.length
    const streetNameLength = streetName.length
    const numberStreetFormat = parseInt(numberStreet)
    const zipCodeFormat = parseInt(zipCode)

    //first check: a "french adress"
    if(!(numberStreetFormat && streetNameLength > 4 && zipCode.length === 5 && cityLength > 1 && zipCodeFormat)) {
        return res.status(401).json(manageErrors(errors.incorrectFields.message,errors.incorrectFields.code))
    }

    //get from public.opendatasoft.com/api district code thanks to city and zip code of a worker
    axios.get(`https://public.opendatasoft.com/api/records/1.0/search/?dataset=correspondance-code-insee-code-postal&q=nom_comm:${city} AND postal_code:${zipCode}`)
    .then((response) => {
        const {code_arr,postal_code } = response.data.records[0].fields
        const codeInseeZip = postal_code.slice(0,2)
        //With district code concatenated it with codeInzeeZip for example 783 will return business unit and district name
        axios.get(`https://data.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-arrondissements-departementaux-2019%40public&q=${codeInseeZip + code_arr}`)
        .then((response) => {
            const {region, nom_de_l_arrondissement} = response.data.records[0].fields
            const results = businessUnitInfo(region,nom_de_l_arrondissement)

            //console.log(results)
            res.status(200).json({results})
        })
        .catch((e) => {
            res.status(401).json(manageErrors(errors.errorRetriveNameDistrict.message, errors.errorRetriveNameDistrict.code, e.message))
        })
    })
    .catch((e) => {
        res.status(401).json(manageErrors(errors.incorrectFieldsForInseeMatchingApi.message, errors.incorrectFieldsForInseeMatchingApi.code, e.message))
    })
}

export default getWorkerAdressInfo

