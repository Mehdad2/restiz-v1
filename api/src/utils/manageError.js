const manageErrors = (message, code, complementErr) => {
    return {
        message,
        code : code,
        complementErr : complementErr ? complementErr : 'none'
    }
}

export default manageErrors
