export default
{
    incorrectFields: {message: "The filled fields seem insufficient for the validation of a French postal address", code : 101},
    incorrectFieldsForInseeMatchingApi : {message: "The fields filled do not make it possible to find a correspondence with the INSEE codes", code : 102},
    errorRetriveNameDistrict : {message: "recovery of district name impossible", code : 201},
}