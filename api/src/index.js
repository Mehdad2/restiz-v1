import express from 'express'
import dotenv from 'dotenv'
import workerAdressRoutes from './routes/workerAdress.routes'
import InitialRoutes from './routes/initial.routes'
import swaggerUi from 'swagger-ui-express'
import ApiswaggerDocument from './api.swagger.json'


dotenv.config()
const app = express()
const PORT = process.env.PORT || 8001

app.set('json spaces', 2);


/**
 * Declare router
 */
//const  ProtectRoutes = express.Router();
const  PublicRoutes = express.Router();


app.use('/api/public', PublicRoutes);

//swagger conf access to api documentation domain-app/api-docs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(ApiswaggerDocument));


/**
 * call to api routes
 */
const Routes = () => {
    InitialRoutes(app)
    workerAdressRoutes(app)
}

Routes()
app.listen(PORT, () => {
    console.log(`Server is listening on PORT ${PORT}`)
});

