import React, {useEffect} from 'react';
import {
  Avatar,
  Button,
  Box,
  Container,
  CssBaseline,
  TextField,
  Typography
} from '@material-ui/core';
import CodeRounded from '@material-ui/icons/CodeRounded';
import  useStyles from '../../styles/workerForm'
import { onInputInfoAdress, setLocalizationWorkerDetails, loadAdressInfo, ThunkGetWorkerDetailAdressInfo } from '../../actions/workerAction'
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner'



 const WorkerAdress = ({ dispatch, adressInfo, loadInfo,businessUnit, nameDistrict, error }) => {
  const classes = useStyles();
  useEffect (() => {
    //dispatch(onInputInfoAdress('',''))
    //dispatch(setLocalizationWorkerDetails({}))
  }, [])

  const onSubmit = async (event) => {
    event.preventDefault()
      dispatch(loadAdressInfo(true))
      dispatch(ThunkGetWorkerDetailAdressInfo())
 
}

const onChange = (event) => {
    const {name, value} = event.target
    dispatch(onInputInfoAdress(name, value));
 }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <CodeRounded />
        </Avatar>
        <Typography component="h1" variant="h5">
          Découvrez à quelle unité et arrondissement vous - êtes rattaché ?
        </Typography>
          <Box color="error.main">
              {error.errors === 102 ? 'Votre adresse est inexistante en base' : ''}
              {error.errors === 101 ? 'Votre adresse ne correspond pas à une adresse postale complète' : ''}
          </Box>
          <Box color="text.secondary">
              {(businessUnit && nameDistrict) ? businessUnit +' '+ nameDistrict : ''}
        </Box>
        <form className={classes.form}  onSubmit={onSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="numberStreet"
            label="Numéro de rue"
            type='number'
            name="numberStreet"
            value={adressInfo.numberStreet}
            onChange={onChange}
            autoComplete="numberStreet"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="streetName"
            label="Nom de la rue"
            type="text"
            id="streetName"
            value={adressInfo.streetName}
            onChange={onChange}
            autoComplete="streetName"
          />
           <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="zipCode"
            label="Code Postal"
            type="number"
            min="5" 
            max="5"
            id="zipCode"
            value={adressInfo.zipCode}
            onChange={onChange}
            autoComplete="zipCode"
          />
         <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="city"
            label="Ville"
            type="text"
            id="city"
            value={adressInfo.city}
            onChange={onChange}
            autoComplete="city"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
              {loadInfo ? <Loader
                  type="TailSpin"
                  color="#00BFFF"
                  height="30"
                  width="30"
              />
                  :
                  'Valider'
              }
          </Button>
        </form>
      </div>
    </Container>
  );
}

// Prendre dans le state des propriétés à donner en props
const mapStateToProps = (state) => ({
    businessUnit: state.user.businessUnit,
    nameDistrict: state.user.nameDistrict,
    loadInfo: state.user.loadInfo,
    error: state.user.errors,
    adressInfo: state.user.adressInfo
   
  });



  
  export default connect(mapStateToProps)(WorkerAdress);