import axios from 'axios'
import {PROTOCOL, DOMAIN} from '../app/utils'
import { workerData } from './types'


export const onInputInfoAdress = (name,value) => ({
    type: workerData.SET_INFO_ADRESS,
    name,
    value
})

export const setLocalizationWorkerDetails = (businessUnit,nameDistrict ) => ({
    type: workerData.SET_LOCALIZATION_WORKER_DETAILS,
    businessUnit,
    nameDistrict

})

export const loadAdressInfo = (loadBool) => ({
    type: workerData.LOAD_ADRESS_INFO,
    loadBool
})
const setErrors = (err) => ({
    type: workerData.ERRORS,
    err
})

/**
 * thunk middleware managing the api call
 * waiting for the response send by the api
 * if sucess return business unit and name district
 * if error return message error
 */

export const ThunkGetWorkerDetailAdressInfo = () => {
    return (dispatch, getState) => {
        const { numberStreet, streetName, zipCode, city  } = getState().user.adressInfo
        axios.get(`${PROTOCOL}://${DOMAIN}/api/public/workers/adress/${numberStreet}/${streetName}/${zipCode}/${city}`)
            .then(async (response) => {
                dispatch(setErrors({}))
                const businessUnit = response.data.results.businessUnit
                const nameDistrict = response.data.results.districtName
                dispatch(setLocalizationWorkerDetails(businessUnit, nameDistrict ))
                dispatch(loadAdressInfo(false))
            })
            .catch((e) => {
                dispatch(setLocalizationWorkerDetails('', '' ))
                dispatch(setErrors(e.response.data.code))
                dispatch(loadAdressInfo(false))
            })

    }
}
