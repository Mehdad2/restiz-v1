import { combineReducers } from 'redux';
import workerInfoReducer from './workerReducer';

//if many reducers...
const createAppReducer = () => combineReducers({
  user: workerInfoReducer,
});


export default createAppReducer;
