import { workerData } from '../actions/types';


/**
 * initial state 
 */
const defaultState = {
    businessUnit: '',
    nameDistrict: '',
    adressInfo: {
        numberStreet: 99,
        streetName: '',
        zipCode: 99999,
        city:''
    },
    loadInfo: false,
    errors: {}
}

const workerReducer = (state = defaultState, action = {}) => {
    switch(action.type) {
        case workerData.SET_LOCALIZATION_WORKER_DETAILS:
            return {
                ...state,
                businessUnit: action.businessUnit,
                nameDistrict: action.nameDistrict
            }
        case workerData.SET_INFO_ADRESS:
            return {
                ...state,
                adressInfo: {
                    ...state.adressInfo,
                    [action.name]: action.value
                }
            }
        case workerData.LOAD_ADRESS_INFO:
            return {
                ...state,
                loadInfo: action.loadBool
            }
        case workerData.ERRORS:
            return {
                ...state,
                errors: {
                    ...state.errors,
                   errors: action.err
            }
        }

        default:
            return state
    }
}

export default workerReducer
