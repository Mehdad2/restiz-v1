import {createStore, applyMiddleware, compose} from 'redux';
import reducer from '../reducers';
import { persistStore, persistReducer } from 'redux-persist'
import ReduxThunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage' 

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = composeEnhancers(applyMiddleware(
    ReduxThunk
));

const persistConfig = {
  key: 'app-react',
  storage,
}


const persistedReducer = persistReducer(persistConfig, reducer())


const store = createStore(
  persistedReducer,
  middleware,
  //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


const persistor = persistStore(store)
window.store = store;

export {store, persistor, persistConfig};